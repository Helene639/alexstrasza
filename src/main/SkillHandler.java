package main;

import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;
import main.java.handlers.CandidatesHandler;
import main.java.handlers.LaunchRequestHandler;
import main.java.handlers.SummaryStartHandler;
import main.java.handlers.StopIntentHandler;
import main.java.handlers.SummaryHandler;

public class SkillHandler  extends SkillStreamHandler {

  public SkillHandler() {
    super(Skills.standard().addRequestHandlers(
        new LaunchRequestHandler(),
        new SummaryStartHandler(),
        new SummaryHandler(),
        new CandidatesHandler(),
        new StopIntentHandler()).build());
  }
}
