package main.java.handlers;

import static com.amazon.ask.request.Predicates.intentName;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import java.util.Optional;

public class StopIntentHandler implements RequestHandler {

  @Override
  public boolean canHandle(HandlerInput handlerInput) {
    return handlerInput.matches(intentName("AMAZON.StopIntent"));
  }

  @Override
  public Optional<Response> handle(HandlerInput handlerInput) {
    String response = "Ok. I'll be here if you need me.";
    return handlerInput.getResponseBuilder().withSpeech(response).withShouldEndSession(true).build();
  }
}
