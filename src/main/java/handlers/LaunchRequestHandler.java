package main.java.handlers;

import static com.amazon.ask.request.Predicates.requestType;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import java.util.Optional;

public class LaunchRequestHandler  implements RequestHandler {

  @Override
  public boolean canHandle(HandlerInput handlerInput) {
    return handlerInput.matches(requestType(LaunchRequest.class));
  }

  @Override
  public Optional<Response> handle(HandlerInput handlerInput) {
    return handlerInput.getResponseBuilder().withSpeech(
        "<audio src='soundbank://soundlibrary/human/amzn_sfx_clear_throat_ahem_01'/>"
            + "Welcome my precioussssss Bobeesha."
            + "<audio src='soundbank://soundlibrary/human/amzn_sfx_crowd_applause_04'/>"
            + "What would you like to do?")
        .withShouldEndSession(false)
        .build();

  }
}
