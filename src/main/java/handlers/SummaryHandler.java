package main.java.handlers;

import static com.amazon.ask.request.Predicates.intentName;
import static com.amazon.ask.request.Predicates.sessionAttribute;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class SummaryHandler implements RequestHandler {
  static String response = "";
  static String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiQUNDRVNTX1RPS0VOIiwicm9sZSI6IkFETUlOIiwidXNlcklkIjo0MDQsImZpcnN0TmFtZSI6IkFsZXhhIiwiZXhwaXJlZEF0IjoiMjAxOC0xMi0yOVQxMTowMzoxNy4yMTQzNTIxIn0.ppJrWwnw6VqhJGf0ZYEe5kyfZmTpkmV3QNFn7AWa0P8";

  @Override
  public boolean canHandle(HandlerInput handlerInput) {
    return handlerInput.matches(intentName("SummaryIntent").and(sessionAttribute("state", "started")));
  }

  @Override
  public Optional<Response> handle(HandlerInput handlerInput) {

    IntentRequest intentRequest = (IntentRequest) handlerInput.getRequestEnvelope().getRequest();
    Map<String, Slot> slots = intentRequest.getIntent().getSlots();

    int n = 0;
    for(Slot slot : slots.values()){
      n = Integer.parseInt(slot.getValue());
    }

    String companyId = Integer.toString(n);

    try {
      response = apiGetSummary(companyId);

    } catch (Exception e) {
      e.printStackTrace();
    }

    return handlerInput.getResponseBuilder().withSpeech(response).withShouldEndSession(false).build();
  }

//  public static String apiFindJavaCandidates() throws IOException, JSONException {
//
//    String url;
//    JSONObject jsonObject;
//    JSONArray jsonArray;
//    String jobId = "";
//    ArrayList<String> javaCandidates = new ArrayList<>();
//    String result = "";
//    String response;
//
//    url = "http://resource-api.edeja.com/v1/jobs";
//    RestTemplate restTemplate = new RestTemplate();
//    HttpHeaders headers = new HttpHeaders();
//    headers.set( "Authorization", "bearer " + accessToken);
//    org.springframework.http.HttpEntity<String> request =
//        new org.springframework.http.HttpEntity<>(headers);
//    ResponseEntity<String> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
//        String.class);
//
//    jsonArray = new JSONArray(responseEntity.getBody());
//
//    for (int i = 0; i < jsonArray.length(); i++){
//      jsonObject = jsonArray.getJSONObject(i);
//      if (jsonObject.getString("position").equalsIgnoreCase("junior java developer")){
//        jobId = jsonObject.getString("id");
//        break;
//      }
//    }
//
//    url = "http://resource-api.edeja.com/v1/job_applications";
//    restTemplate = new RestTemplate();
//    request = new org.springframework.http.HttpEntity<>(headers);
//    responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
//        String.class);
//
//    jsonArray = new JSONArray(responseEntity.getBody());
//
//    for (int i = 0; i < jsonArray.length(); i++){
//      jsonObject = jsonArray.getJSONObject(i);
//      if (jsonObject.getString("jobId").equalsIgnoreCase(jobId)){
//        javaCandidates.add(jsonObject.getString("candidateEmail"));
//      }
//    }
//
//    for (int i = 0; i < javaCandidates.size(); i++) {
//      if (i == javaCandidates.size() - 1){
//        result += javaCandidates.get(i) + ".";
//      }
//      else{
//        result += javaCandidates.get(i) + ", ";
//      }
//    }
//
//    response = "Ok, I've found " + Integer.toString(javaCandidates.size()) + " Java candidates. Their emails are: " + result;
//
//    return response;
//  }

  public static String apiGetSummary(String companyId) throws JSONException {
    Random rnd = new Random();
    String url;
    JSONObject jsonObject;
    String response;

    url = "http://resource-api.edeja.com/v1/summary/company/" + companyId;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set( "Authorization", "bearer " + accessToken);
    org.springframework.http.HttpEntity<String> request =
        new org.springframework.http.HttpEntity<>(headers);
    ResponseEntity<String> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
        String.class);

    jsonObject = new JSONObject(responseEntity.getBody());
    String openJobPositions = jsonObject.getString("openJobPositions");
    String totalJobApplications = jsonObject.getString("totalJobApplications");
    String reviewedJobApplications = jsonObject.getString("reviewedJobApplications");
    String unreviewedJobApplications = jsonObject.getString("unreviewedJobApplications");

    int n = rnd.nextInt((3 - 1) + 1) + 1;
    String reprompt = "";

    switch (n){
      case 1:
        reprompt = "What would you like to do next?";
        break;

      case 2:
        reprompt = "Is there anything else I can do for you?";
        break;

      case 3:
        reprompt = "Is there anything else you need?";
        break;
    }

    response = "I've found the company with the specified id. The total number of open job positions"
        + " for this company is " + openJobPositions + ", and it currently has " + totalJobApplications
        + " job applications, out of which " + reviewedJobApplications + " are reviewed and "
        + unreviewedJobApplications + " are not. "
        + reprompt;

    return response;
  }

//  public static void main(String[] args) throws IOException, JSONException {
//    System.out.println(apiFindJavaCandidates());
//    System.out.println(apiGetSummary("800"));
//  }

}
