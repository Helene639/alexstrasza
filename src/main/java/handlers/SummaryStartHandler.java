package main.java.handlers;

import static com.amazon.ask.request.Predicates.intentName;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import java.util.Map;
import java.util.Optional;

public class SummaryStartHandler implements RequestHandler {
  @Override
  public boolean canHandle(HandlerInput input) {
    return input.matches(intentName("SummaryStartIntent"));
  }

  @Override
  public Optional<Response> handle(HandlerInput input) {
    Map<String, Object> sessionAttributes = input.getAttributesManager().getSessionAttributes();
    sessionAttributes.put("state", "started");

    return input.getResponseBuilder()
        .withSpeech("Which company id would you like the summary for?")
        .withShouldEndSession(false)
        .build();
  }

}
