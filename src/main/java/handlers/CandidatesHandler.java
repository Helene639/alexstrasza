package main.java.handlers;

import static com.amazon.ask.request.Predicates.intentName;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CandidatesHandler implements RequestHandler {
  static String response = "";
  static String accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiQUNDRVNTX1RPS0VOIiwicm9sZSI6IkFETUlOIiwidXNlcklkIjo0MDQsImZpcnN0TmFtZSI6IkFsZXhhIiwiZXhwaXJlZEF0IjoiMjAxOC0xMi0yOVQxMTowMzoxNy4yMTQzNTIxIn0.ppJrWwnw6VqhJGf0ZYEe5kyfZmTpkmV3QNFn7AWa0P8";

  @Override
  public boolean canHandle(HandlerInput handlerInput) {
    return handlerInput.matches(intentName("CandidatesIntent"));
  }

  @Override
  public Optional<Response> handle(HandlerInput handlerInput) {

    try {
      response = apiFindJavaCandidates();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return handlerInput.getResponseBuilder().withSpeech(response).withShouldEndSession(false).build();
  }

  public static String apiFindJavaCandidates() throws IOException, JSONException {

    String url;
    JSONObject jsonObject;
    JSONArray jsonArray;
    String jobId = "";
    ArrayList<String> javaCandidates = new ArrayList<>();
    String result = "";
    String response;

    url = "http://resource-api.edeja.com/v1/jobs";
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set( "Authorization", "bearer " + accessToken);
    org.springframework.http.HttpEntity<String> request =
        new org.springframework.http.HttpEntity<>(headers);
    ResponseEntity<String> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
        String.class);

    jsonArray = new JSONArray(responseEntity.getBody());

    for (int i = 0; i < jsonArray.length(); i++){
      jsonObject = jsonArray.getJSONObject(i);
      if (jsonObject.getString("position").equalsIgnoreCase("junior java developer")){
        jobId = jsonObject.getString("id");
        break;
      }
    }

    url = "http://resource-api.edeja.com/v1/job_applications";
    restTemplate = new RestTemplate();
    request = new org.springframework.http.HttpEntity<>(headers);
    responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
        String.class);

    jsonArray = new JSONArray(responseEntity.getBody());

    for (int i = 0; i < jsonArray.length(); i++){
      jsonObject = jsonArray.getJSONObject(i);
      if (jsonObject.getString("jobId").equalsIgnoreCase(jobId)){
        javaCandidates.add(jsonObject.getString("candidateEmail"));
      }
    }

    for (int i = 0; i < javaCandidates.size(); i++) {
      if (i == javaCandidates.size() - 1){
        result += javaCandidates.get(i) + ".";
      }
      else{
        result += javaCandidates.get(i) + ", ";
      }
    }

    Random rnd = new Random();
    String reprompt = "";
    int n = rnd.nextInt((3 - 1) + 1) + 1;

    switch (n){
      case 1:
        reprompt = "What would you like to do next?";
        break;

      case 2:
        reprompt = "Is there anything else I can do for you?";
        break;

      case 3:
        reprompt = "Is there anything else you need?";
        break;
    }

    response = "Ok, I've found " + Integer.toString(javaCandidates.size()) +
        " Java candidates. Their emails are: " + result + " " + reprompt;


//======================
//    if (json != null || !json.isEmpty()) {
//      response = "Ok, I've found a job with the id " + id + ". The position of the job is " + o.get("position") + " and the current salary is " + o.get("salary") + " " + o.get("salaryCurrency") + ".";
//    } else {
//      response = "We are fucked!";
// =====================

    return response;
  }

  public static String apiGetSummary(String companyId) throws JSONException {
    String url;
    JSONObject jsonObject;
    String response;

    url = "http://resource-api.edeja.com/v1/summary/company/" + companyId;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set( "Authorization", "bearer " + accessToken);
    org.springframework.http.HttpEntity<String> request =
        new org.springframework.http.HttpEntity<>(headers);
    ResponseEntity<String> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, request,
        String.class);

    jsonObject = new JSONObject(responseEntity.getBody());
    String openJobPositions = jsonObject.getString("openJobPositions");
    String totalJobApplications = jsonObject.getString("totalJobApplications");
    String reviewedJobApplications = jsonObject.getString("reviewedJobApplications");
    String unreviewedJobApplications = jsonObject.getString("unreviewedJobApplications");

    response = "I've found the company with the specified id. The total number of open job positions"
        + " for this company is " + openJobPositions + ", and it currently has " + totalJobApplications
        + " job applications, out of which " + reviewedJobApplications + " are reviewed and "
        + unreviewedJobApplications + " are not.";

    return response;
  }

  public static void main(String[] args) throws IOException, JSONException {
    System.out.println(apiFindJavaCandidates());
    System.out.println(apiGetSummary("800"));
  }

}
